#!/usr/bin/zsh
temp=`waybar-weather | awk -F° '{print $1}' | tail -c 3`
if [ "$temp" -ge 33 ]; then
out="☀"
elif [ "$temp" -lt 33 ] && [ "$temp" -ge 23 ]; then
out="☼"
elif [ "$temp" -lt 23 ] && [ "$temp" -ge 0 ]; then
out="☁" 
elif [ "$temp" -lt 0 ]; then
out="❄"
fi

echo $out

